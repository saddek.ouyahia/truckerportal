import { Link, useLocation } from "@remix-run/react";
import logo from "../../assets/logo_truck_bo.png";
import { ConfigProvider, Dropdown, MenuProps, Switch, Space, Menu, Avatar } from "antd";
import { MdDarkMode, MdLightMode } from "react-icons/md";
import '../../tailwind.css';
import { RiLogoutBoxRLine } from "react-icons/ri";
import Cookies from "js-cookie";
import { FaChevronDown } from "react-icons/fa";
import { useEffect, useState } from 'react';

const Navbar = ({ toggleTheme, isDarkMode }: { toggleTheme: () => void; isDarkMode: boolean }) => {
	const location = useLocation();
	const isActive = (pathname: string) => location.pathname === pathname;
	const [userData, setUserData] = useState(null);

	const handleLogout = () => {
		Cookies.remove('truckersmp_session');
		Cookies.remove('userData');
		window.location.href = '/';
	};

	useEffect(() => {
		const userDataCookie = Cookies.get('userData');
		if (userDataCookie) {
			setUserData(JSON.parse(userDataCookie));
		}
	}, []);

	const items: MenuProps['items'] = [
		{
			label: <span>Afficher le profil</span>,
			key: '0',
		},
		{
			type: 'divider',
			key: '1',
		},
		{
			label: (
				<ConfigProvider
					theme={{
						token: {
							colorPrimary: '#2a475e',
						},
					}}
				>
					Theme:
					<Switch
						size={"small"}
						className="custom-switch ml-2"
						checkedChildren={<MdDarkMode size={16} />}
						unCheckedChildren={<MdLightMode size={16} />}
						checked={isDarkMode}
						onChange={toggleTheme}
					/>
				</ConfigProvider>
			),
			key: '2',
		},
		{
			type: 'divider',
			key: '3',
		},
		{
			label: (
				<button className="flex items-center hover:text-[red]" onClick={handleLogout}>
					<RiLogoutBoxRLine size={22} className="mr-1" /> Logout
				</button>
			),
			key: '4',
		},
	];

	return (
		<nav className="bg-[#23c0a6] fixed top-0 left-0 right-0 z-10 flex items-center justify-between px-4 py-4 shadow-2xl">
			<div className="flex items-center">
				<img src={logo} alt="Logo" className="h-[63px] w-auto" />
				<div className="ml-16 space-x-6">
					<Link to="/" className={`text-lg font-semibold pr-8 ${isActive('/') ? 'text-[#2a475e]' : 'text-white hover:text-[#fff1dc]'}`}>Accueil</Link>
					<Link to="/promods" className={`text-lg font-semibold pr-8 ${isActive('/promods') ? 'text-[#2a475e]' : 'text-white hover:text-[#fff1dc]'}`}>Promods</Link>
					<Link to="/about" className={`text-lg font-semibold pr-8 ${isActive('/about') ? 'text-[#2a475e]' : 'text-white hover:text-[#fff1dc]'}`}>À propos</Link>
				</div>
			</div>
			<div className="flex items-center space-x-4">
				{userData && (
					<Dropdown overlay={<Menu items={items} />} trigger={['hover']}>
						<a onClick={(e) => e.preventDefault()}>
							<Space>
								<Avatar
									src={userData.smallAvatar}
									alt="Avatar"
									style={{
										border: '1px solid white',
										padding: '1px',
										margin: '0',
									}}
								/>
								<span className={"text-white"}>{userData.name}</span>
								<FaChevronDown className={"text-[#2a475e]"}/>
							</Space>
						</a>
					</Dropdown>
				)}
				{!userData && (
					<ConfigProvider
						theme={{
							token: {
								colorPrimary: '#2a475e',
							},
						}}
					>
						<Switch
							className="custom-switch"
							checkedChildren={<MdDarkMode size={20} />}
							unCheckedChildren={<MdLightMode size={20} />}
							checked={isDarkMode}
							onChange={toggleTheme}
						/>
					</ConfigProvider>
				)}
			</div>
		</nav>
	);
};

export default Navbar;