import { FaSteam } from "react-icons/fa";

export default function Index() {
	return (
		<div className="font-sans p-4 flex flex-row justify-center items-stretch space-x-4 h-screen">

			<div className="w-1/2 space-y-6 flex flex-col justify-center items-center">
				<form className="space-y-6 w-full max-w-lg p-10 shadow-2xl rounded-lg border border-gray-200">
					<h1 className="text-3xl text-center mb-12">Accéder à TruckersPortal</h1>
					<button
						type="button"
						className="flex items-center justify-center bg-[#1b2838] hover:bg-[#2a475e] text-[#ffffff] font-bold py-2 px-4 rounded w-full"
					>
						<FaSteam className="mr-3" size={25} />
						Juste tester
					</button>
					<div className="relative">
						<div className="absolute inset-0 flex items-center" aria-hidden="true">
							<div className="w-full border-t border-gray-300"></div>
						</div>
						<div className="relative flex justify-center">
							<span className="px-2 bg-white text-sm text-gray-500">OU</span>
						</div>
					</div>
					<input
						type="text"
						placeholder="Steam ID ou TruckersMP ID"
						className="border border-[#2a475e] p-2 rounded w-full"
					/>
					<button
						type="submit"
						className="mt-4 w-full bg-[#23c0a6] hover:bg-teal-600 text-[#ffffff] font-bold py-2 px-4 rounded">
						Se connecter
					</button>
				</form>
			</div>
		</div>
	);
}
