import { useState } from 'react';
import { DotLottieReact } from '@lottiefiles/dotlottie-react';
import { FaSteam } from "react-icons/fa";
import ets2Logo from "~/assets/est2_Logo.png";
import atsLogo from "~/assets/ats_Logo.png";
import Cookies from 'js-cookie'; // Import js-cookie

export default function Index() {
	const [userId, setUserId] = useState('');
	const [userData, setUserData] = useState(null);
	const [error, setError] = useState('');

	const handleInputChange = (event) => {
		setUserId(event.target.value);
	};

	const handleSubmit = async (event) => {
		if (!userId){
			setError('Veuillez entrer un ID valide.');
			return;
		}
		event.preventDefault();
		try {
			const response = await fetch(`/api/player/${userId}`);
			if (response.ok) {
				const data = await response.json();
				console.log('Data:', data);
				setUserData(data);
				Cookies.set('userData', JSON.stringify(data.response), { expires: 7 });
				setError('');
			} else {
				const status = response.status;
				let message = 'Une erreur est survenue';
				if (status === 404) {
					message = 'Utilisateur non trouvé. Veuillez vérifier votre ID.';
				} else if (status === 500) {
					message = 'Erreur interne du serveur. Veuillez réessayer plus tard.';
				}
				setError(message);
			}
		} catch (error) {
			setError('Erreur lors de la récupération des données. Veuillez vérifier votre connexion.');
			console.error('Fetching error:', error);
		}
	};

	return (
		<div className="font-sans p-4 flex flex-row justify-center items-stretch space-x-4 h-screen">
			<div className="w-1/2 flex flex-col p-10 justify-center">
				<div className="w-full flex justify-center">
					<DotLottieReact
						autoplay
						loop
						src="https://lottie.host/bebfb8a8-d9e5-47f0-93a3-518c2cd98d9b/EXvltEoSvP.json"
						style={{ width: '100%', height: 'auto' }}
					/>
				</div>
			</div>
			<div className="w-1/2 space-y-6 flex flex-col justify-center items-center">
				<form className="space-y-6 w-full max-w-lg p-10 shadow-2xl rounded-lg border border-gray-200">
					{error && <p className="text-center justify-center bg-red-500 text-white rounded p-2">{error}</p>}
					<h1 className="text-3xl text-center">Accéder à TruckersPortal</h1>
					<div className="flex justify-center space-x-4 pb-4">
						<img src={ets2Logo} alt="ETS2 Logo" className="w-40 h-auto" />
						<img src={atsLogo} alt="ATS Logo" className="w-40 h-auto" />
					</div>
					<button type="button"
									className="flex items-center justify-center bg-[#1b2838] hover:bg-[#2a475e] text-[#ffffff] font-bold py-2 px-4 rounded w-full border border-[#ffffff]"
									onClick={() => window.location.href = '/auth/steam'}>
						<FaSteam className="mr-3" size={25} />
						Se connecter avec Steam
					</button>
					<div className="relative">
						<div className="absolute inset-0 flex items-center" aria-hidden="true">
							<div className="w-full border-t border-gray-300"></div>
						</div>
						<div className="relative flex justify-center text-sm">
							<span className="px-2 bg-white text-gray-500">OU</span>
						</div>
					</div>
					<input
						type="text"
						value={userId}
						onChange={handleInputChange}
						placeholder="Steam ID ou TruckersMP ID"
						className="border border-[#2a475e] p-2 rounded w-full"
						style={{ color: 'var(--input-foreground)', backgroundColor: 'var(--input)' }}
					/>
					<button type="button" onClick={handleSubmit}
									className="mt-4 w-full bg-[#23c0a6] hover:bg-teal-600 text-[#ffffff] font-bold py-2 px-4 rounded">
						Se connecter
					</button>
				</form>
			</div>
		</div>
	);
}