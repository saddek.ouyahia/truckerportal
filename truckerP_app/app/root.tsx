import { useState } from "react";
import { Links, Meta, Outlet, Scripts, ScrollRestoration } from "@remix-run/react";
import { ConfigProvider, theme } from "antd";
import { ClientOnly } from "remix-utils/client-only";
import "./tailwind.css";
import Navbar from "~/components/ui/navbar";

export default function App() {
  const { defaultAlgorithm, darkAlgorithm } = theme;
  const [isDarkMode, setIsDarkMode] = useState(false);

  const toggleTheme = () => {
    setIsDarkMode(prev => !prev);
  };

  return (
    <html lang="fr" className={isDarkMode ? 'dark' : ''}>
    <head>
      <meta charSet="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <Meta />
      <Links />
    </head>
    <body>
    <ConfigProvider theme={{ algorithm: isDarkMode ? darkAlgorithm : defaultAlgorithm }}>
      <ClientOnly fallback={null}>
        {() => (
          <>
            <Navbar toggleTheme={toggleTheme} isDarkMode={isDarkMode} />
            <Outlet />
          </>
        )}
      </ClientOnly>
    </ConfigProvider>
    <ScrollRestoration />
    <Scripts />
    </body>
    </html>
  );
}