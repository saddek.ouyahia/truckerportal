import Cookies from "js-cookie";

export function isLogged() {
	return !!Cookies.get('userData');
}

export function getUserData() {
	return Cookies.get('userData');
}